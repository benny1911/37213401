﻿CREATE TABLE [dbo].[Coupons] (
    [_store]             VARCHAR (50) NOT NULL,
    [_dueDate]           DATE         NOT NULL,
    [_description]       VARCHAR (50) NOT NULL,
    [_avgRating]         REAL         CONSTRAINT [DF_Coupons__avgRating] DEFAULT ((0)) NOT NULL,
    [_availableQuantity] int		   NOT NULL,
    [_category]          VARCHAR (50) NOT NULL,
    [_originalPrice]     DOUBLE         NOT NULL,
    [_newPrice]          DOUBLE         NOT NULL,
    CONSTRAINT [PK_Coupons] PRIMARY KEY CLUSTERED ([_store] ASC, [_dueDate] ASC, [_description] ASC)
);

