﻿CREATE TABLE [dbo].[UsersCoupons] (
    [_userID]      INT          NOT NULL,
    [_store]       VARCHAR (50) NOT NULL,
    [_dueDate]     DATE         NOT NULL,
    [_description] VARCHAR (50) NOT NULL,
    [_dateUsed]    DATE         NULL,
    [_rating]      INT          NULL,
    CONSTRAINT [PK_UsersCoupons] PRIMARY KEY CLUSTERED ([_userID] ASC, [_store] ASC, [_dueDate] ASC, [_description] ASC),
    CONSTRAINT [FK_UsersCoupons_Coupons] FOREIGN KEY ([_store], [_dueDate], [_description]) REFERENCES [dbo].[Coupons] ([_store], [_dueDate], [_description]),
    CONSTRAINT [FK_UsersCoupons_Users] FOREIGN KEY ([_userID]) REFERENCES [dbo].[Users] ([_ID])
);

