﻿CREATE TABLE [dbo].[Coupons] (
    [_store]             VARCHAR (50) NOT NULL,
    [_dueDate]           DATE         NOT NULL,
    [_description]       VARCHAR (50) NOT NULL,
    [_avgRating]         REAL         CONSTRAINT [DF_Coupons__avgRating] DEFAULT ((0)) NOT NULL,
    [_availableQuantity] NCHAR (10)   NOT NULL,
    [_category]          VARCHAR (50) NOT NULL,
    [_originalPrice]     REAL         NOT NULL,
    [_newPrice]          REAL         NOT NULL,
    CONSTRAINT [PK_Coupons] PRIMARY KEY CLUSTERED ([_store] ASC, [_dueDate] ASC, [_description] ASC),
	CONSTRAINT [FK_store] FOREIGN KEY (_store) REFERENCES [Stores]([_name])
);

