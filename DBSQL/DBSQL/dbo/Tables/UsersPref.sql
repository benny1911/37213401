﻿CREATE TABLE [dbo].[UsersPref] (
    [_ID]         INT        NOT NULL,
    [_preference] NCHAR (10) NOT NULL,
    CONSTRAINT [PK_UsersPref] PRIMARY KEY CLUSTERED ([_ID] ASC, [_preference] ASC),
    CONSTRAINT [id] FOREIGN KEY ([_ID]) REFERENCES [dbo].[Users] ([_ID])
);

