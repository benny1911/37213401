﻿CREATE TABLE [dbo].[Users] (
    [_ID]             INT          NOT NULL,
    [_name]           VARCHAR (50) NOT NULL,
    [_password]       VARCHAR (50) NOT NULL,
    [_authentication] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([_ID] ASC), 
);

