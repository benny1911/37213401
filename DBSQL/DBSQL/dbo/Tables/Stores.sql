﻿CREATE TABLE [dbo].[Stores] (
    [_name]     VARCHAR (50) NOT NULL,
    [_location] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Stores] PRIMARY KEY CLUSTERED ([_name] ASC)
);

