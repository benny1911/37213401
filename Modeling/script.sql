USE [nituz]
GO
/****** Object:  Table [dbo].[Coupons]    Script Date: 29-Mar-15 5:45:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Coupons](
	[_store] [varchar](50) NOT NULL,
	[_dueDate] [date] NOT NULL,
	[_description] [varchar](50) NOT NULL,
	[_avgRating] [real] NOT NULL,
	[_availableQuantity] [nchar](10) NOT NULL,
	[_category] [varchar](50) NOT NULL,
	[_originalPrice] [real] NOT NULL,
	[_newPrice] [real] NOT NULL,
 CONSTRAINT [PK_Coupons] PRIMARY KEY CLUSTERED 
(
	[_store] ASC,
	[_dueDate] ASC,
	[_description] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Stores]    Script Date: 29-Mar-15 5:45:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Stores](
	[_name] [varchar](50) NOT NULL,
	[_location] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Stores] PRIMARY KEY CLUSTERED 
(
	[_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 29-Mar-15 5:45:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[_ID] [int] NOT NULL,
	[_name] [varchar](50) NOT NULL,
	[_password] [varchar](50) NOT NULL,
	[_authentication] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UsersCoupons]    Script Date: 29-Mar-15 5:45:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UsersCoupons](
	[_userID] [int] NOT NULL,
	[_store] [varchar](50) NOT NULL,
	[_dueDate] [date] NOT NULL,
	[_description] [varchar](50) NOT NULL,
	[_dateUsed] [date] NULL,
	[_rating] [int] NULL,
 CONSTRAINT [PK_UsersCoupons] PRIMARY KEY CLUSTERED 
(
	[_userID] ASC,
	[_store] ASC,
	[_dueDate] ASC,
	[_description] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UsersPref]    Script Date: 29-Mar-15 5:45:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersPref](
	[_ID] [int] NOT NULL,
	[_preference] [nchar](10) NOT NULL,
 CONSTRAINT [PK_UsersPref] PRIMARY KEY CLUSTERED 
(
	[_ID] ASC,
	[_preference] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Coupons] ADD  CONSTRAINT [DF_Coupons__avgRating]  DEFAULT ((0)) FOR [_avgRating]
GO
ALTER TABLE [dbo].[UsersCoupons]  WITH CHECK ADD  CONSTRAINT [FK_UsersCoupons_Coupons] FOREIGN KEY([_store], [_dueDate], [_description])
REFERENCES [dbo].[Coupons] ([_store], [_dueDate], [_description])
GO
ALTER TABLE [dbo].[UsersCoupons] CHECK CONSTRAINT [FK_UsersCoupons_Coupons]
GO
ALTER TABLE [dbo].[UsersCoupons]  WITH CHECK ADD  CONSTRAINT [FK_UsersCoupons_Users] FOREIGN KEY([_userID])
REFERENCES [dbo].[Users] ([_ID])
GO
ALTER TABLE [dbo].[UsersCoupons] CHECK CONSTRAINT [FK_UsersCoupons_Users]
GO
ALTER TABLE [dbo].[UsersPref]  WITH CHECK ADD  CONSTRAINT [id] FOREIGN KEY([_ID])
REFERENCES [dbo].[Users] ([_ID])
GO
ALTER TABLE [dbo].[UsersPref] CHECK CONSTRAINT [id]
GO
