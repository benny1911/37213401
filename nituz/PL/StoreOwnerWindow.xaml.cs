﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_BackEnd;
namespace PL
{
    /// <summary>
    /// Interaction logic for StoreOwnerWindow.xaml
    /// </summary>
    public partial class StoreOwnerWindow : Window
    {
        StoreOwnerBL itsOwnerBL;
        UserBL itsUserBL;
        clsUser user;
        string name;
        int userID;
        public StoreOwnerWindow(StoreOwnerBL isOwnerBL, int userid, clsUser curruUser, UserBL isUserBL)
        {
            itsOwnerBL = isOwnerBL;
            user = curruUser;
            userID = userid;
            itsUserBL = isUserBL;
            InitializeComponent();
        }

        private void MenuItem_Coupon(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = e.Source as MenuItem;
            switch (menuItem.Name)
            {
                case "Add": { AddRegularCoupon(); } break;
                case "Look": { LookForAcoupon(); } break;
                case "Rank": { RankACoupon(); } break;
                case "View": { ViewCoupon(); } break;
                case "Purchased": { PurchasedCoupon(); } break;
                case "exit": { this.Visibility = System.Windows.Visibility.Collapsed; System.Environment.Exit(1); return; } 
            }
        }

        private void PurchasedCoupon()
        {
             /* collapes every window:*/
            RankCouponWindow.Visibility = System.Windows.Visibility.Collapsed;
            AddCoupon.Visibility = System.Windows.Visibility.Collapsed;
            ViewCouponWindow.Visibility = System.Windows.Visibility.Collapsed;
            LookCoupon.Visibility = System.Windows.Visibility.Collapsed;
            ViewMyStorePurchasedCoupons.Visibility = System.Windows.Visibility.Collapsed;
            /* show only this window: */
            ViewMyStorePurchasedCoupons.Visibility = System.Windows.Visibility.Visible;
            couponsPurchasedView.ItemsSource = itsOwnerBL.showBoughtCoupons(name);
        }


        private void ViewCoupon()
        {
            /* collapes every window:*/
            RankCouponWindow.Visibility = System.Windows.Visibility.Collapsed;
            ViewMyStorePurchasedCoupons.Visibility = System.Windows.Visibility.Collapsed;
            AddCoupon.Visibility = System.Windows.Visibility.Collapsed;
            ViewCouponWindow.Visibility = System.Windows.Visibility.Collapsed;
            LookCoupon.Visibility = System.Windows.Visibility.Collapsed;

            /* show only this window: */
            ViewCouponWindow.Visibility = System.Windows.Visibility.Visible;
            couponsView.ItemsSource = itsUserBL.searchCoupon(userID, false, null, null);
        }

        private void RankACoupon()
        {
            /* collapes every window:*/
            ViewCouponWindow.Visibility = System.Windows.Visibility.Collapsed;
            AddCoupon.Visibility = System.Windows.Visibility.Collapsed;
            ViewMyRankedPurchasedCoupons.Visibility = System.Windows.Visibility.Collapsed;
            ViewMyStorePurchasedCoupons.Visibility = System.Windows.Visibility.Collapsed;
            LookCoupon.Visibility = System.Windows.Visibility.Collapsed;
            RankCouponWindow.Visibility = System.Windows.Visibility.Collapsed;

            /* show only this window: */
            RankCouponWindow.Visibility = System.Windows.Visibility.Visible;
        }

        private void LookForAcoupon()
        {
            ViewCouponWindow.Visibility = System.Windows.Visibility.Collapsed;
            RankCouponWindow.Visibility = System.Windows.Visibility.Collapsed;
            AddCoupon.Visibility = System.Windows.Visibility.Collapsed;
            LookCoupon.Visibility = System.Windows.Visibility.Collapsed;

            /* show only this window: */
            LookCoupon.Visibility = System.Windows.Visibility.Visible;
       
        }

        private void AddRegularCoupon()
        {
            ViewCouponWindow.Visibility = System.Windows.Visibility.Collapsed;
            RankCouponWindow.Visibility = System.Windows.Visibility.Collapsed;
            LookCoupon.Visibility = System.Windows.Visibility.Collapsed;
            AddCoupon.Visibility = System.Windows.Visibility.Collapsed;

            /* show only this window: */
            AddCoupon.Visibility = System.Windows.Visibility.Visible;
            name = user.getName();
            newCouponSocialName.Text = name;
        }

        private void btn_addCouponFromSocial(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime dueDate = DateTime.Parse(newCouponSocialDateEnd.Text);
                string description = newCouponSocialDesc.Text;
                int quantity = Int32.Parse(newCouponSocialQuantity.Text);
                string category = newCouponSocialCategory.Text;
                double originalPrice = Double.Parse(newCouponSocialOrgPrice.Text);
                double newPrice = Double.Parse(newCouponSocialNewPrice.Text);

                bool legal = itsOwnerBL.addCoupon(name, dueDate, description, quantity, category, originalPrice, newPrice);
                if (!legal) throw new Exception();
            }
            catch
            {
                MessageBox.Show("sorry wrong input!");
                return;
            }
            MessageBox.Show("Success!");
        }

        private void btn_searchCoupon(object sender, RoutedEventArgs e)
        {
            try
            {
                string storeName = lookStore.Text;

                if (storeName.Equals(""))
                {
                    storeName = null;
                }
                string location = lookStore.Text;
                if (location.Equals(""))
                {
                    location = null;
                }
                bool preferences = false;
                if (lookByPref.IsChecked == true)
                {
                    preferences = true;
                }
                couponsToBuyGrid.ItemsSource = itsUserBL.searchCoupon(userID, preferences, storeName, location);
            }
            catch
            {
                MessageBox.Show("sorry wrong input!");
            }
        }

        private void btn_buyCoupon(object sender, RoutedEventArgs e)
        {
            try
            {
                var selected = (clsCoupon)this.couponsToBuyGrid.SelectedItem;
                string storeName = selected.getStore();
                DateTime due = selected.getDueDate();
                string desc = selected.getDescription();
                string mail = itsUserBL.getMail(userID);
                if (mail == "")
                {
                    throw new Exception();
                }
                itsUserBL.buyCoupon(userID, storeName, due, desc, mail);
                storeName = lookStore.Text;
                if (storeName.Equals(""))
                {
                    storeName = null;
                }
                string location = lookStore.Text;
                if (location.Equals(""))
                {
                    location = null;
                }
                bool preferences = false;
                if (lookByPref.IsChecked == true)
                {
                    preferences = true;
                }
                couponsToBuyGrid.ItemsSource = itsUserBL.searchCoupon(userID, preferences, storeName, location);
            }
            catch
            {
                MessageBox.Show("sorry wrong input!");
            }
        }

        private void btn_rateCoupon(object sender, RoutedEventArgs e)
        {
            try
            {
                var selected = (clsUsersCoupon)this.boughtCouponsGrid.SelectedItem;
                int rate = Int32.Parse(ratingCouponBox.Text);
                if (rate < 1 | 5 < rate)
                {
                    throw new Exception();
                }
                int success = itsUserBL.rateCoupon(userID, selected.getStore(), selected.getDueDate(), selected.getDescription(), rate);
                if (success == -1)
                {
                    throw new Exception();
                }
            }
            catch
            {
                MessageBox.Show("sorry wrong input!");
                return;
            }
            boughtCouponsGrid.ItemsSource = itsUserBL.viewOrders(userID);
        }
    }
}
