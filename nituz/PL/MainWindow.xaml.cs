﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DAL;
using BL_BackEnd;
using BL;

namespace PL
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int userID;
        private string pass;
        private SQL_DAL_implementation iDAL;
        private UserBL itsUserBL;
        private AdminBL itsAdminBL;
        private StoreOwnerBL itsOwnerBL;
        //private string auth;

        public MainWindow()
        {
            iDAL = new SQL_DAL_implementation();
            InitializeComponent();
            itsUserBL = new UserBL(iDAL);
            itsAdminBL = new AdminBL(iDAL);
            this.Show(); 
        }

        private void LoginPressed(object sender, RoutedEventArgs e)
        {
            /* validate input */
            try
            {
                 userID = Convert.ToInt32(ID.Text);
                 pass = Password.Password;
            }
            catch
            {
                MessageBox.Show("ID must contains digits");
                return;
            }
            
            string auth = itsUserBL.getAuthorization(userID, pass);

            /* validate user */
            if (auth!="A" && auth!="U" && auth!="S")
            {
                MessageBox.Show("User does not exist");
                return;
            }
            else
            {
                clsUser user = iDAL.getUserByID(userID);
                MessageBox.Show("Welcome " + user.getName() + " !");
                  if (auth == "A")
                  {
                      AdminWindow aw = new AdminWindow(iDAL, userID, itsUserBL);
                    this.Close();
                    aw.Show();
                  }
                  else if (auth == "U")
                 {
                     UserWindow uw = new UserWindow(iDAL, userID, itsAdminBL);
                    this.Close();
                    uw.Show();
                 }
                 else
                 {
                     itsOwnerBL = new StoreOwnerBL(iDAL);
                     StoreOwnerWindow sow = new StoreOwnerWindow(itsOwnerBL,userID, user,itsUserBL);    
                      this.Close();
                      sow.Show();
                 }
            }
        }

        private void ForgorPasswordPressed(object sender, RoutedEventArgs e)
        {
            RestorePasswordWindow rpw = new RestorePasswordWindow(itsUserBL);
            this.Close();
            rpw.ShowDialog();
        }

        private void SignUpUser(object sender, RoutedEventArgs e)
        {
            SignUpWindow suw = new SignUpWindow(itsUserBL, iDAL, itsAdminBL);
            this.Close();
            suw.ShowDialog();
        }

    }
}
