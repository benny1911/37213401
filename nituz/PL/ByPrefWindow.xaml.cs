﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_BackEnd;

namespace PL
{
    /// <summary>
    /// Interaction logic for ByPrefWindow.xaml
    /// </summary>
    public partial class ByPrefWindow : Window
    {
        UserBL uBl;
        public ByPrefWindow(BL.UserBL temp)
        {
            InitializeComponent();
            uBl = temp;
        }

        private void enterId(object sender, RoutedEventArgs e)
        {
            int id = Convert.ToInt32(ID.Text);
            uBl.searchCoupon(id, true, null, null);
            this.Close();
        }

        private void IDInput(object sender, TextChangedEventArgs e)
        {

        }
    }
}
