﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="coupon")]
	public partial class DataClasses1DataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertCoupon(Coupon instance);
    partial void UpdateCoupon(Coupon instance);
    partial void DeleteCoupon(Coupon instance);
    partial void InsertStore(Store instance);
    partial void UpdateStore(Store instance);
    partial void DeleteStore(Store instance);
    partial void InsertUser(User instance);
    partial void UpdateUser(User instance);
    partial void DeleteUser(User instance);
    partial void InsertUsersCoupon(UsersCoupon instance);
    partial void UpdateUsersCoupon(UsersCoupon instance);
    partial void DeleteUsersCoupon(UsersCoupon instance);
    partial void InsertUsersPref(UsersPref instance);
    partial void UpdateUsersPref(UsersPref instance);
    partial void DeleteUsersPref(UsersPref instance);
    #endregion
		
		public DataClasses1DataContext() : 
				base(global::DAL.Properties.Settings.Default.couponConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<Coupon> Coupons
		{
			get
			{
				return this.GetTable<Coupon>();
			}
		}
		
		public System.Data.Linq.Table<Store> Stores
		{
			get
			{
				return this.GetTable<Store>();
			}
		}
		
		public System.Data.Linq.Table<User> Users
		{
			get
			{
				return this.GetTable<User>();
			}
		}
		
		public System.Data.Linq.Table<UsersCoupon> UsersCoupons
		{
			get
			{
				return this.GetTable<UsersCoupon>();
			}
		}
		
		public System.Data.Linq.Table<UsersPref> UsersPrefs
		{
			get
			{
				return this.GetTable<UsersPref>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Coupons")]
	public partial class Coupon : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private string @__store;
		
		private System.DateTime @__dueDate;
		
		private string @__description;
		
		private float @__avgRating;
		
		private int @__availableQuantity;
		
		private string @__category;
		
		private float @__originalPrice;
		
		private float @__newPrice;
		
		private System.Nullable<bool> @__isAuthByAdmin;
		
		private EntitySet<UsersCoupon> _UsersCoupons;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void On_storeChanging(string value);
    partial void On_storeChanged();
    partial void On_dueDateChanging(System.DateTime value);
    partial void On_dueDateChanged();
    partial void On_descriptionChanging(string value);
    partial void On_descriptionChanged();
    partial void On_avgRatingChanging(float value);
    partial void On_avgRatingChanged();
    partial void On_availableQuantityChanging(int value);
    partial void On_availableQuantityChanged();
    partial void On_categoryChanging(string value);
    partial void On_categoryChanged();
    partial void On_originalPriceChanging(float value);
    partial void On_originalPriceChanged();
    partial void On_newPriceChanging(float value);
    partial void On_newPriceChanged();
    partial void On_isAuthByAdminChanging(System.Nullable<bool> value);
    partial void On_isAuthByAdminChanged();
    #endregion
		
		public Coupon()
		{
			this._UsersCoupons = new EntitySet<UsersCoupon>(new Action<UsersCoupon>(this.attach_UsersCoupons), new Action<UsersCoupon>(this.detach_UsersCoupons));
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_store]", Storage="__store", DbType="VarChar(50) NOT NULL", CanBeNull=false, IsPrimaryKey=true)]
		public string _store
		{
			get
			{
				return this.@__store;
			}
			set
			{
				if ((this.@__store != value))
				{
					this.On_storeChanging(value);
					this.SendPropertyChanging();
					this.@__store = value;
					this.SendPropertyChanged("_store");
					this.On_storeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_dueDate]", Storage="__dueDate", DbType="Date NOT NULL", IsPrimaryKey=true)]
		public System.DateTime _dueDate
		{
			get
			{
				return this.@__dueDate;
			}
			set
			{
				if ((this.@__dueDate != value))
				{
					this.On_dueDateChanging(value);
					this.SendPropertyChanging();
					this.@__dueDate = value;
					this.SendPropertyChanged("_dueDate");
					this.On_dueDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_description]", Storage="__description", DbType="VarChar(50) NOT NULL", CanBeNull=false, IsPrimaryKey=true)]
		public string _description
		{
			get
			{
				return this.@__description;
			}
			set
			{
				if ((this.@__description != value))
				{
					this.On_descriptionChanging(value);
					this.SendPropertyChanging();
					this.@__description = value;
					this.SendPropertyChanged("_description");
					this.On_descriptionChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_avgRating]", Storage="__avgRating", DbType="Real NOT NULL")]
		public float _avgRating
		{
			get
			{
				return this.@__avgRating;
			}
			set
			{
				if ((this.@__avgRating != value))
				{
					this.On_avgRatingChanging(value);
					this.SendPropertyChanging();
					this.@__avgRating = value;
					this.SendPropertyChanged("_avgRating");
					this.On_avgRatingChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_availableQuantity]", Storage="__availableQuantity", DbType="Int NOT NULL")]
		public int _availableQuantity
		{
			get
			{
				return this.@__availableQuantity;
			}
			set
			{
				if ((this.@__availableQuantity != value))
				{
					this.On_availableQuantityChanging(value);
					this.SendPropertyChanging();
					this.@__availableQuantity = value;
					this.SendPropertyChanged("_availableQuantity");
					this.On_availableQuantityChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_category]", Storage="__category", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string _category
		{
			get
			{
				return this.@__category;
			}
			set
			{
				if ((this.@__category != value))
				{
					this.On_categoryChanging(value);
					this.SendPropertyChanging();
					this.@__category = value;
					this.SendPropertyChanged("_category");
					this.On_categoryChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_originalPrice]", Storage="__originalPrice", DbType="Real NOT NULL")]
		public float _originalPrice
		{
			get
			{
				return this.@__originalPrice;
			}
			set
			{
				if ((this.@__originalPrice != value))
				{
					this.On_originalPriceChanging(value);
					this.SendPropertyChanging();
					this.@__originalPrice = value;
					this.SendPropertyChanged("_originalPrice");
					this.On_originalPriceChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_newPrice]", Storage="__newPrice", DbType="Real NOT NULL")]
		public float _newPrice
		{
			get
			{
				return this.@__newPrice;
			}
			set
			{
				if ((this.@__newPrice != value))
				{
					this.On_newPriceChanging(value);
					this.SendPropertyChanging();
					this.@__newPrice = value;
					this.SendPropertyChanged("_newPrice");
					this.On_newPriceChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_isAuthByAdmin]", Storage="__isAuthByAdmin", DbType="Bit")]
		public System.Nullable<bool> _isAuthByAdmin
		{
			get
			{
				return this.@__isAuthByAdmin;
			}
			set
			{
				if ((this.@__isAuthByAdmin != value))
				{
					this.On_isAuthByAdminChanging(value);
					this.SendPropertyChanging();
					this.@__isAuthByAdmin = value;
					this.SendPropertyChanged("_isAuthByAdmin");
					this.On_isAuthByAdminChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Coupon_UsersCoupon", Storage="_UsersCoupons", ThisKey="_store,_dueDate,_description", OtherKey="_store,_dueDate,_description")]
		public EntitySet<UsersCoupon> UsersCoupons
		{
			get
			{
				return this._UsersCoupons;
			}
			set
			{
				this._UsersCoupons.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_UsersCoupons(UsersCoupon entity)
		{
			this.SendPropertyChanging();
			entity.Coupon = this;
		}
		
		private void detach_UsersCoupons(UsersCoupon entity)
		{
			this.SendPropertyChanging();
			entity.Coupon = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Stores")]
	public partial class Store : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private string @__name;
		
		private string @__location;
		
		private string @__address;
		
		private string @__description;
		
		private string @__cagtegory;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void On_nameChanging(string value);
    partial void On_nameChanged();
    partial void On_locationChanging(string value);
    partial void On_locationChanged();
    partial void On_addressChanging(string value);
    partial void On_addressChanged();
    partial void On_descriptionChanging(string value);
    partial void On_descriptionChanged();
    partial void On_cagtegoryChanging(string value);
    partial void On_cagtegoryChanged();
    #endregion
		
		public Store()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_name]", Storage="__name", DbType="VarChar(50) NOT NULL", CanBeNull=false, IsPrimaryKey=true)]
		public string _name
		{
			get
			{
				return this.@__name;
			}
			set
			{
				if ((this.@__name != value))
				{
					this.On_nameChanging(value);
					this.SendPropertyChanging();
					this.@__name = value;
					this.SendPropertyChanged("_name");
					this.On_nameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_location]", Storage="__location", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string _location
		{
			get
			{
				return this.@__location;
			}
			set
			{
				if ((this.@__location != value))
				{
					this.On_locationChanging(value);
					this.SendPropertyChanging();
					this.@__location = value;
					this.SendPropertyChanged("_location");
					this.On_locationChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_address]", Storage="__address", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string _address
		{
			get
			{
				return this.@__address;
			}
			set
			{
				if ((this.@__address != value))
				{
					this.On_addressChanging(value);
					this.SendPropertyChanging();
					this.@__address = value;
					this.SendPropertyChanged("_address");
					this.On_addressChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_description]", Storage="__description", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string _description
		{
			get
			{
				return this.@__description;
			}
			set
			{
				if ((this.@__description != value))
				{
					this.On_descriptionChanging(value);
					this.SendPropertyChanging();
					this.@__description = value;
					this.SendPropertyChanged("_description");
					this.On_descriptionChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_cagtegory]", Storage="__cagtegory", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string _cagtegory
		{
			get
			{
				return this.@__cagtegory;
			}
			set
			{
				if ((this.@__cagtegory != value))
				{
					this.On_cagtegoryChanging(value);
					this.SendPropertyChanging();
					this.@__cagtegory = value;
					this.SendPropertyChanged("_cagtegory");
					this.On_cagtegoryChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Users")]
	public partial class User : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int @__ID;
		
		private string @__name;
		
		private string @__password;
		
		private string @__authentication;
		
		private string @__initialLocation;
		
		private System.Nullable<int> @__notificationStyle;
		
		private bool @__shouldBeNotified;
		
		private string @__email;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void On_IDChanging(int value);
    partial void On_IDChanged();
    partial void On_nameChanging(string value);
    partial void On_nameChanged();
    partial void On_passwordChanging(string value);
    partial void On_passwordChanged();
    partial void On_authenticationChanging(string value);
    partial void On_authenticationChanged();
    partial void On_initialLocationChanging(string value);
    partial void On_initialLocationChanged();
    partial void On_notificationStyleChanging(System.Nullable<int> value);
    partial void On_notificationStyleChanged();
    partial void On_shouldBeNotifiedChanging(bool value);
    partial void On_shouldBeNotifiedChanged();
    partial void On_emailChanging(string value);
    partial void On_emailChanged();
    #endregion
		
		public User()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_ID]", Storage="__ID", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int _ID
		{
			get
			{
				return this.@__ID;
			}
			set
			{
				if ((this.@__ID != value))
				{
					this.On_IDChanging(value);
					this.SendPropertyChanging();
					this.@__ID = value;
					this.SendPropertyChanged("_ID");
					this.On_IDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_name]", Storage="__name", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string _name
		{
			get
			{
				return this.@__name;
			}
			set
			{
				if ((this.@__name != value))
				{
					this.On_nameChanging(value);
					this.SendPropertyChanging();
					this.@__name = value;
					this.SendPropertyChanged("_name");
					this.On_nameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_password]", Storage="__password", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string _password
		{
			get
			{
				return this.@__password;
			}
			set
			{
				if ((this.@__password != value))
				{
					this.On_passwordChanging(value);
					this.SendPropertyChanging();
					this.@__password = value;
					this.SendPropertyChanged("_password");
					this.On_passwordChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_authentication]", Storage="__authentication", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string _authentication
		{
			get
			{
				return this.@__authentication;
			}
			set
			{
				if ((this.@__authentication != value))
				{
					this.On_authenticationChanging(value);
					this.SendPropertyChanging();
					this.@__authentication = value;
					this.SendPropertyChanged("_authentication");
					this.On_authenticationChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_initialLocation]", Storage="__initialLocation", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string _initialLocation
		{
			get
			{
				return this.@__initialLocation;
			}
			set
			{
				if ((this.@__initialLocation != value))
				{
					this.On_initialLocationChanging(value);
					this.SendPropertyChanging();
					this.@__initialLocation = value;
					this.SendPropertyChanged("_initialLocation");
					this.On_initialLocationChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_notificationStyle]", Storage="__notificationStyle", DbType="Int")]
		public System.Nullable<int> _notificationStyle
		{
			get
			{
				return this.@__notificationStyle;
			}
			set
			{
				if ((this.@__notificationStyle != value))
				{
					this.On_notificationStyleChanging(value);
					this.SendPropertyChanging();
					this.@__notificationStyle = value;
					this.SendPropertyChanged("_notificationStyle");
					this.On_notificationStyleChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_shouldBeNotified]", Storage="__shouldBeNotified", DbType="Bit NOT NULL")]
		public bool _shouldBeNotified
		{
			get
			{
				return this.@__shouldBeNotified;
			}
			set
			{
				if ((this.@__shouldBeNotified != value))
				{
					this.On_shouldBeNotifiedChanging(value);
					this.SendPropertyChanging();
					this.@__shouldBeNotified = value;
					this.SendPropertyChanged("_shouldBeNotified");
					this.On_shouldBeNotifiedChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_email]", Storage="__email", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string _email
		{
			get
			{
				return this.@__email;
			}
			set
			{
				if ((this.@__email != value))
				{
					this.On_emailChanging(value);
					this.SendPropertyChanging();
					this.@__email = value;
					this.SendPropertyChanged("_email");
					this.On_emailChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.UsersCoupons")]
	public partial class UsersCoupon : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int @__userID;
		
		private string @__store;
		
		private System.DateTime @__dueDate;
		
		private string @__description;
		
		private System.Nullable<System.DateTime> @__dateUsed;
		
		private System.Nullable<int> @__rating;
		
		private string @__authKey;
		
		private System.Nullable<bool> @__authenticatedByStore;
		
		private bool @__isUsed;
		
		private EntityRef<Coupon> _Coupon;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void On_userIDChanging(int value);
    partial void On_userIDChanged();
    partial void On_storeChanging(string value);
    partial void On_storeChanged();
    partial void On_dueDateChanging(System.DateTime value);
    partial void On_dueDateChanged();
    partial void On_descriptionChanging(string value);
    partial void On_descriptionChanged();
    partial void On_dateUsedChanging(System.Nullable<System.DateTime> value);
    partial void On_dateUsedChanged();
    partial void On_ratingChanging(System.Nullable<int> value);
    partial void On_ratingChanged();
    partial void On_authKeyChanging(string value);
    partial void On_authKeyChanged();
    partial void On_authenticatedByStoreChanging(System.Nullable<bool> value);
    partial void On_authenticatedByStoreChanged();
    partial void On_isUsedChanging(bool value);
    partial void On_isUsedChanged();
    #endregion
		
		public UsersCoupon()
		{
			this._Coupon = default(EntityRef<Coupon>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_userID]", Storage="__userID", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int _userID
		{
			get
			{
				return this.@__userID;
			}
			set
			{
				if ((this.@__userID != value))
				{
					this.On_userIDChanging(value);
					this.SendPropertyChanging();
					this.@__userID = value;
					this.SendPropertyChanged("_userID");
					this.On_userIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_store]", Storage="__store", DbType="VarChar(50) NOT NULL", CanBeNull=false, IsPrimaryKey=true)]
		public string _store
		{
			get
			{
				return this.@__store;
			}
			set
			{
				if ((this.@__store != value))
				{
					this.On_storeChanging(value);
					this.SendPropertyChanging();
					this.@__store = value;
					this.SendPropertyChanged("_store");
					this.On_storeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_dueDate]", Storage="__dueDate", DbType="Date NOT NULL", IsPrimaryKey=true)]
		public System.DateTime _dueDate
		{
			get
			{
				return this.@__dueDate;
			}
			set
			{
				if ((this.@__dueDate != value))
				{
					this.On_dueDateChanging(value);
					this.SendPropertyChanging();
					this.@__dueDate = value;
					this.SendPropertyChanged("_dueDate");
					this.On_dueDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_description]", Storage="__description", DbType="VarChar(50) NOT NULL", CanBeNull=false, IsPrimaryKey=true)]
		public string _description
		{
			get
			{
				return this.@__description;
			}
			set
			{
				if ((this.@__description != value))
				{
					this.On_descriptionChanging(value);
					this.SendPropertyChanging();
					this.@__description = value;
					this.SendPropertyChanged("_description");
					this.On_descriptionChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_dateUsed]", Storage="__dateUsed", DbType="Date")]
		public System.Nullable<System.DateTime> _dateUsed
		{
			get
			{
				return this.@__dateUsed;
			}
			set
			{
				if ((this.@__dateUsed != value))
				{
					this.On_dateUsedChanging(value);
					this.SendPropertyChanging();
					this.@__dateUsed = value;
					this.SendPropertyChanged("_dateUsed");
					this.On_dateUsedChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_rating]", Storage="__rating", DbType="Int")]
		public System.Nullable<int> _rating
		{
			get
			{
				return this.@__rating;
			}
			set
			{
				if ((this.@__rating != value))
				{
					this.On_ratingChanging(value);
					this.SendPropertyChanging();
					this.@__rating = value;
					this.SendPropertyChanged("_rating");
					this.On_ratingChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_authKey]", Storage="__authKey", DbType="VarChar(50)")]
		public string _authKey
		{
			get
			{
				return this.@__authKey;
			}
			set
			{
				if ((this.@__authKey != value))
				{
					this.On_authKeyChanging(value);
					this.SendPropertyChanging();
					this.@__authKey = value;
					this.SendPropertyChanged("_authKey");
					this.On_authKeyChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_authenticatedByStore]", Storage="__authenticatedByStore", DbType="Bit")]
		public System.Nullable<bool> _authenticatedByStore
		{
			get
			{
				return this.@__authenticatedByStore;
			}
			set
			{
				if ((this.@__authenticatedByStore != value))
				{
					this.On_authenticatedByStoreChanging(value);
					this.SendPropertyChanging();
					this.@__authenticatedByStore = value;
					this.SendPropertyChanged("_authenticatedByStore");
					this.On_authenticatedByStoreChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_isUsed]", Storage="__isUsed", DbType="Bit NOT NULL")]
		public bool _isUsed
		{
			get
			{
				return this.@__isUsed;
			}
			set
			{
				if ((this.@__isUsed != value))
				{
					this.On_isUsedChanging(value);
					this.SendPropertyChanging();
					this.@__isUsed = value;
					this.SendPropertyChanged("_isUsed");
					this.On_isUsedChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Coupon_UsersCoupon", Storage="_Coupon", ThisKey="_store,_dueDate,_description", OtherKey="_store,_dueDate,_description", IsForeignKey=true)]
		public Coupon Coupon
		{
			get
			{
				return this._Coupon.Entity;
			}
			set
			{
				Coupon previousValue = this._Coupon.Entity;
				if (((previousValue != value) 
							|| (this._Coupon.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._Coupon.Entity = null;
						previousValue.UsersCoupons.Remove(this);
					}
					this._Coupon.Entity = value;
					if ((value != null))
					{
						value.UsersCoupons.Add(this);
						this.@__store = value._store;
						this.@__dueDate = value._dueDate;
						this.@__description = value._description;
					}
					else
					{
						this.@__store = default(string);
						this.@__dueDate = default(System.DateTime);
						this.@__description = default(string);
					}
					this.SendPropertyChanged("Coupon");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.UsersPref")]
	public partial class UsersPref : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int @__ID;
		
		private string @__preference;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void On_IDChanging(int value);
    partial void On_IDChanged();
    partial void On_preferenceChanging(string value);
    partial void On_preferenceChanged();
    #endregion
		
		public UsersPref()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_ID]", Storage="__ID", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int _ID
		{
			get
			{
				return this.@__ID;
			}
			set
			{
				if ((this.@__ID != value))
				{
					this.On_IDChanging(value);
					this.SendPropertyChanging();
					this.@__ID = value;
					this.SendPropertyChanged("_ID");
					this.On_IDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[_preference]", Storage="__preference", DbType="NChar(10) NOT NULL", CanBeNull=false, IsPrimaryKey=true)]
		public string _preference
		{
			get
			{
				return this.@__preference;
			}
			set
			{
				if ((this.@__preference != value))
				{
					this.On_preferenceChanging(value);
					this.SendPropertyChanging();
					this.@__preference = value;
					this.SendPropertyChanged("_preference");
					this.On_preferenceChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
