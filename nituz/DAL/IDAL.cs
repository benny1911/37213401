﻿using BL_BackEnd;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public interface IDAL
    {
       //add region
       //!!!!!VERY IMPORTANT : all function declaration in IDAL are located at the same way in implementation calsses!
       int addStore(string name, string location, string address, string description, string category);
       int addUser(int id, string name, string pass, string auth, int notificationStyle, string initialLocation, string email);
       int addUsersPref(int id, string pref);
       int addCoupon(string store, DateTime duedate, string description, int availibleQuantity, string category, double originalPrice, double newPrice, bool authByAdmin);
       int addUsersCoupon(int id, string store, DateTime duedate, string desc, string validationKey);

       //delete region
       int removeUsersCoupon(int idToRemove, string store, DateTime due, string desc);
       int removeUser(int idToRemove);
       int removeStore(string name, string location);
       int removeCoupon(string store, DateTime due, string desc);
       int removeUsersPref(int id, string pref);

       //update region
       int updateCoupon(string store, DateTime due, string desc, DateTime newTime, string newDesc, int avgRating, int quantity, string category, int oldPrice, int newPrice, bool isAuthByAdmin);
       int updateUsersCoupon(int userId, string store, DateTime due, string desc, DateTime useTime, int rating, bool authByStore, bool isUsed);
       int updateUsersPref(int userId, string pref, string newPref);
       int updateUser(int userId, string newName, string newPass, string newAuth, int notificationStyle, bool shouldBeNotified, string location, string email);
       int updateStore(string name, string newName, string newLoc, string newAddress, string newDescription, string newCategory);

       //get single region
       clsUser getUserByID(int id);
       clsStore getStoreByName(string name);
       clsUsersCoupon getUsersCouponByKey(int id, string store, DateTime due, string desc);
       clsCoupon getCouponByKey(string store, DateTime due, string desc);

       //get lists region
       List<clsUser> getAllUsers();
       List<clsUsersPref> getAllUsersPref();
       List<clsStore> getAllStores();
       List<clsUsersCoupon> getAllUsersCoupon();
       List<clsCoupon> getAllCoupons();
       List<clsUsersPref> getAllUsersPrefByID(int id);
       List<clsUsersCoupon> getAllUsersCouponsByID(int id);
       List<clsUsersCoupon> getUsersCouponsByStore(string store);
       List<clsCoupon> getCouponsByStore(string store);
       List<clsUser> getUsersByPref(string pref);
       List<clsCoupon> getCouponsNotAuthorized();
    }
}
