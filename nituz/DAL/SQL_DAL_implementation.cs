﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;
using BL_BackEnd;

namespace DAL
{

    public class SQL_DAL_implementation : IDAL
    {
        //fields:
        public SqlConnection myConnection { get; set; }
        public DataClasses1DataContext db;

        #region initialization
        //constructor:
        public SQL_DAL_implementation()
        {
            this.db = new DataClasses1DataContext();
            SQLConnect();
        }

        //methods:
        public void SQLConnect()
        {
            string path = Directory.GetCurrentDirectory();
            string path2 = path.Substring(0, path.Length - 20);

            myConnection = new SqlConnection("Data Source=(LocalDB)\\v11.0;" +
                                        "AttachDbFilename=\"" + path2 + "\\DAL\\coupon.mdf" + "\";" +
                                         "Integrated Security=True;" +
                                         "Connect Timeout=20");



            try
            {
                myConnection.Open();
            }

            catch (Exception e1)
            {
                throw e1;
            }
        }
        public void SQLWrite(String commandText)
        {
            SqlCommand command = new SqlCommand(commandText, myConnection);
            command.ExecuteNonQuery();//throws exceptions
        }

        #endregion
        #region add-functions
        /// <summary>
        /// add a store into the database
        /// </summary>
        /// <param name="name">name of the store</param>
        /// <param name="location">location of the store, currently implemented by string</param>
        /// <returns>returns 1 upon success, -1 upon fail</returns>
        public int addStore(string name, string location, string address, string description, string category)
        {
            string commandText = "INSERT INTO Stores " + "([_name],[_location],[_address],[_description],[_cagtegory])" +

              "Values ('" + name + "','" + location + "','" + address + "','" + description + "','" + category + "')";

            try
            {
                SQLWrite(commandText);
                return 1;
            }
            catch
            {
                return -1;
            }

        }
        /// <summary>
        /// adds a user to the database
        /// </summary>
        /// <param name="id">users id</param>
        /// <param name="name">users name</param>
        /// <param name="pass">users pass, must be at least 4 digits long</param>
        /// <param name="auth">"A"-admin,"U"-user,"S"-store owner</param>
        /// <param name="notificationStyle"> notification style - 0 for all, 1 for location, 2 for users pref. defualt 0</param>
        /// <param name="initialLocation"> the initial location of the user</param>
        /// <returns>1 upon success -1 otherwise</returns>
        public int addUser(int id, string name, string pass, string auth,int notificationStyle, string initialLocation, string email)
        {
            if (pass.Length < 4) return -1;
            if (!auth.Equals("A") && !auth.Equals("U") && !auth.Equals("S")) return -1;

            bool shouldBeNotified = true;
            string commandText = "INSERT INTO [dbo].[Users] " + "([_ID],[_name],[_password],[_authentication],[_initialLocation],[_notificationStyle],[_shouldBeNotified],[_email])" +

              " Values ('" + id + "','" + name + "','" + pass + "','" + auth + "','" + initialLocation + "','" + notificationStyle + "','" + shouldBeNotified + "','" + email + "')";

            try
            {
                SQLWrite(commandText);
                return 1;
            }
            catch
            {
                return -1;
            }

        }
        /// <summary>
        /// adds users preference into db
        /// </summary>
        /// <param name="id">existing user id</param>
        /// <param name="pref">users preference</param>
        /// <returns>1 upon seccess, -1 otherwise.</returns>
        public int addUsersPref(int id, string pref)
        {
            string commandText = "INSERT INTO [dbo].[UsersPref] " + "([_ID],[_preferences])" +

              "Values ('" + id + "','" + pref + "')";

            try
            {
                SQLWrite(commandText);
                return 1;
            }
            catch
            {
                return -1;
            }

        }
        /// <summary>
        /// adds coupon into the database
        /// </summary>
        /// <param name="store">stores name</param>
        /// <param name="duedate">due date of the coupon</param>
        /// <param name="description">description of the coupon</param>
        /// <param name="availibleQuantity">availible quantity</param>
        /// <param name="category">category of the coupon</param>
        /// <param name="originalPrice">original price</param>
        /// <param name="newPrice">new price </param>
        /// <param name="authByAdmin">0 for not authorized yet, 1 otherwise </param>
        /// <returns>1 upon seccess, -1 otherwise.</returns>
        public int addCoupon(string store, DateTime duedate, string description, int availibleQuantity, string category, double originalPrice, double newPrice,bool authByAdmin)
        {
            double avgRating = 0;
            string commandText = "INSERT INTO [dbo].[Coupons] " + "([_store],[_dueDate],[_description],[_avgRating],[_availableQuantity],[_category],[_originalPrice],[_newPrice],[_isAuthByAdmin])" +

              "Values ('" + store + "','" + duedate + "','" + description + "','" + avgRating + "','" + availibleQuantity + "','" + category + "','" + originalPrice + "','" + newPrice + "','" + authByAdmin + "')";
            try
            {
                SQLWrite(commandText);
                return 1;
            }
            catch
            {
                return -1;
            }

        }
        /// <summary>
        /// adds new coupon to users coupons. 
        /// assumption: the use date of the coupon is 1.1.9999 when unused. 
        /// users rating = 3 until the user decides to change the rating 
        /// </summary>
        /// <param name="id">users id</param>
        /// <param name="store">stores name</param>
        /// <param name="duedate">due date of the coupon</param>
        /// <param name="desc">description of the coupon</param>
        /// <param name="validationKey">a random validation key as string</param>
        /// <returns>1 upon seccess, -1 otherwise.</returns>
        public int addUsersCoupon(int id, string store, DateTime duedate, string desc,string validationKey)
        {
            double rating = 0;
            DateTime dateUsed = new DateTime(9999, 1, 1);
            string commandText = "INSERT INTO UsersCoupons " + "([_userID],[_store],[_dueDate],[_description],[_dateUsed],[_rating],[_authKey],[_authenticatedByStore],[_isUsed])" +

              "Values ('" + id + "','" + store + "','" + duedate + "','" + desc + "','" + dateUsed + "','" + rating + "','" + validationKey + "','" + false + "','" + false +"')";
            try
            {
                SQLWrite(commandText);
                return 1;
            }
            catch
            {
                return -1;
            }

        }
        #endregion
        #region delete
        /// <summary>
        /// remove users coupon from db
        /// </summary>
        /// <param name="idToRemove">if og the user</param>
        /// <param name="store">stores name</param>
        /// <param name="due">due date of the coupon. should be dd/mm/yyyy</param>
        /// <param name="desc">description of the coupon</param>
        /// <returns>returns 1 upon success, -1 upon fail</returns>
        public int removeUsersCoupon(int idToRemove, string store, DateTime due, string desc)
        {
            foreach (UsersCoupon curr in db.UsersCoupons)
            {
                if ((curr._userID == idToRemove) &&
                                                (curr._store.Equals(store)) &&
                                                                        (curr._dueDate.Year == due.Year) && (curr._dueDate.Month == due.Month) && (curr._dueDate.Day == due.Day) &&
                                                                                                                                                                        (desc.Equals(curr._description)))
                {
                    db.UsersCoupons.DeleteOnSubmit(curr);
                    break;
                }
            }

            try
            {
                db.SubmitChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        /// <summary>
        /// removes a user from DB. also removes usersPref and usersCoupon.
        /// </summary>
        /// <param name="idToRemove">user to remove</param>
        /// <returns>1 upon succeess, -1 otherwise</returns>
        public int removeUser(int idToRemove)
        {
            foreach (User curr in db.Users)
            {
                if (curr._ID == idToRemove)
                {
                    if (removeUsersCouponById(idToRemove) != 1 || removeUsersPrefById(idToRemove) != 1) return -1;
                    db.Users.DeleteOnSubmit(curr);
                    break;
                }
            }

            try
            {
                db.SubmitChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        /// <summary>
        /// removes all coupons by a given user
        /// </summary>
        /// <param name="idToRemove">users id</param>
        /// <returns>1 upon success, -1 otherwise</returns>
        private int removeUsersCouponById(int idToRemove)
        {
            foreach (UsersCoupon curr in db.UsersCoupons)
            {
                if (curr._userID == idToRemove)
                    db.UsersCoupons.DeleteOnSubmit(curr);
            }

            try
            {
                db.SubmitChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        /// <summary>
        /// removes all of users pref from db
        /// </summary>
        /// <param name="idToRemove">users id to be removed</param>
        /// <returns>1 upon success, -1 otherwise</returns>
        private int removeUsersPrefById(int idToRemove)
        {
            foreach (UsersPref curr in db.UsersPrefs)
            {
                if (curr._ID == idToRemove)
                    db.UsersPrefs.DeleteOnSubmit(curr);
            }
            try
            {
                db.SubmitChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        /// <summary>
        /// removes a store by its name and location
        /// additionaly, removes all the coupons from Coupons table
        /// </summary>
        /// <param name="name">stores name</param>
        /// <param name="location">stores location</param>
        /// <returns>1 upon success, -1 otherwise</returns>
        public int removeStore(string name, string location)
        {
            foreach (Store curr in db.Stores)
            {
                if (curr._name.Equals(name) && curr._location.Equals(location))
                {
                    if (removeCouponsByStore(name) != 1) return -1;
                    db.Stores.DeleteOnSubmit(curr);
                    break;
                }
            }

            try
            {
                db.SubmitChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        /// <summary>
        /// removes all coupons that yet to be removed when a store removed from db
        /// </summary>
        /// <param name="name">stores name</param>
        /// <returns>1 upon success, -1 otherwise</returns>
        private int removeCouponsByStore(string name)
        {
            foreach (Coupon curr in db.Coupons)
            {
                if (curr._store.Equals(name))
                {
                    db.Coupons.DeleteOnSubmit(curr);
                }
            }

            try
            {
                db.SubmitChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        /// <summary>
        /// deletes a coupon from db
        /// </summary>
        /// <param name="store"> store name </param>
        /// <param name="due">due date of the coupon</param>
        /// <param name="desc">description of the coupon</param>
        /// <returns>1 upon success, -1 otherwise</returns>
        public int removeCoupon(string store, DateTime due, string desc)
        {
            foreach (Coupon curr in db.Coupons)
            {
                if ((curr._store.Equals(store)) &&
                                              (curr._dueDate.Year == due.Year) && (curr._dueDate.Month == due.Month) && (curr._dueDate.Day == due.Day) &&
                                                                                                                                                (desc.Equals(curr._description)))
                {
                    db.Coupons.DeleteOnSubmit(curr);
                    break;
                }
            }

            try
            {
                db.SubmitChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        /// <summary>
        /// removes usersPref from the db
        /// </summary>
        /// <param name="id">users id</param>
        /// <param name="pref">preference</param>
        /// <returns>1 upon success, -1 otherwise</returns>
        public int removeUsersPref(int id, string pref)
        {
            foreach (UsersPref curr in db.UsersPrefs)
            {
                if ((curr._ID.Equals(id)) && (curr._preference.Equals(pref)))
                {
                    db.UsersPrefs.DeleteOnSubmit(curr);
                    break;
                }
            }

            try
            {
                db.SubmitChanges();
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        #endregion
        #region update
        /// <summary>
        /// updates coupon,note the store can't be updated. values that should not be changes should be sent by their old values
        /// </summary>
        /// <param name="store">the store that identifies the coupon</param>
        /// <param name="due">current due date</param>
        /// <param name="desc">current descrption</param>
        /// <param name="newTime">new due date of the coupon</param>
        /// <param name="newDesc">new desc</param>
        /// <param name="avgRating">new avg rating of the coupon so far</param>
        /// <param name="quantity">new quantity</param>
        /// <param name="category">new category of the coupon</param>
        /// <param name="oldPrice">should always stay unchanged</param>
        /// <param name="newPrice">new price to be updated</param>
        /// <param name="isAuthByAdmin">change iff the admin authorized the coupon from a user</param>
        /// <returns>1 upon success ,-1 otherwise</returns>
        public int updateCoupon(string store, DateTime due, string desc, DateTime newTime, string newDesc, int avgRating, int quantity, string category, int oldPrice, int newPrice,bool isAuthByAdmin)
        {
            foreach (Coupon curr in db.Coupons)
            {
                if ((curr._store.Equals(store)) &&
                         (curr._dueDate.Year == due.Year) && (curr._dueDate.Month == due.Month) && (curr._dueDate.Day == due.Day) &&
                                 (desc.Equals(curr._description)))
                {
                    curr._dueDate = newTime;
                    curr._description = newDesc;
                    curr._avgRating = avgRating;
                    curr._availableQuantity = quantity;
                    curr._category = category;
                    curr._originalPrice = oldPrice;
                    curr._newPrice = newPrice;
                    curr._isAuthByAdmin = isAuthByAdmin;
                    try { db.SubmitChanges(); return 1; }
                    catch { return -1; }
                }

            }
            return -1;


        }
        /// <summary>
        /// updates the users coupon, can only update useTime and rating
        /// serial key of the coupon is given once at creation
        /// </summary>
        /// <param name="userId">users id who bought the coupon</param>
        /// <param name="store">the store from where the coupon came</param>
        /// <param name="due">due date of the coupon</param>
        /// <param name="desc">description of the coupon</param>
        /// <param name="useTime">the date when the user used the coupon</param>
        /// <param name="rating">the rating that the user gave to the coupon</param>
        /// <param name="authByStore">if the store authenticated the coupon</param>
        /// <returns>1 upon success, -1 otherwise</returns>
        public int updateUsersCoupon(int userId, string store, DateTime due, string desc, DateTime useTime, int rating,bool authByStore, bool isUsed)
        {
            foreach (UsersCoupon curr in db.UsersCoupons)
            {
                if ((curr._userID == userId) &&
                    (curr._store.Equals(store)) &&
                     (curr._dueDate.Year == due.Year) && (curr._dueDate.Month == due.Month) && (curr._dueDate.Day == due.Day) &&
                      (desc.Equals(curr._description)))
                {
                    curr._dateUsed = useTime;
                    curr._rating = rating;
                    curr._authenticatedByStore = authByStore;
                    curr._isUsed = isUsed;
                    break;
                }

            }
            try { db.SubmitChanges(); return 1; }
            catch { return -1; }

        }
        /// <summary>
        /// updates the users preference
        /// </summary>
        /// <param name="userId">the user id that his preference should be updated</param>
        /// <param name="pref">the current pref to update</param>
        /// <param name="newPref">the new pref</param>
        /// <returns>1 upon success , -1 otherwise</returns>
        public int updateUsersPref(int userId, string pref, string newPref)
        {
            foreach (UsersPref curr in db.UsersPrefs)
            {
                if ((curr._ID == userId) &&
                    (curr._preference.Equals(pref)))
                {
                    curr._preference = pref;
                    break;
                }

            }
            try { db.SubmitChanges(); return 1; }
            catch { return -1; }
        }
        /// <summary>
        /// updates users profile
        /// </summary>
        /// <param name="userId">users id to be identefied by </param>
        /// <param name="newName">users new name</param>
        /// <param name="newPass">users new pass</param>
        /// <param name="newAuth">users new auth. "A","S","U"</param>
        /// <param name="notificationStyle">the new notigication style</param>
        /// <returns>1 upon success, -1 otherwise</returns>
        public int updateUser(int userId, string newName, string newPass, string newAuth, int notificationStyle,bool shouldBeNotified,string location, string email)
        {
            foreach (User curr in db.Users)
            {
                if (curr._ID == userId)
                {
                    curr._name = newName;
                    curr._password = newPass;
                    curr._authentication = newAuth;
                    curr._notificationStyle = notificationStyle;
                    curr._shouldBeNotified = shouldBeNotified;
                    curr._initialLocation = location;
                    curr._email = email;
                    break;
                }

            }
            try { db.SubmitChanges(); return 1; }
            catch { return -1; }
        }
        /// <summary>
        /// updates stores data
        /// </summary>
        /// <param name="name">old name of the store</param>
        /// <param name="location">old location of the store</param>
        /// <param name="newName">new name for the store</param>
        /// <param name="newLoc">new location for the store</param>
        /// <returns>1 upon success, -1 otherwise</returns>
        public int updateStore(string name, string newName, string newLoc, string newAddress, string newDescription, string newCategory)
        {
            foreach (Store curr in db.Stores)
            {
                if (curr._name.Equals(name) )
                {
                    curr._name = newName;
                    curr._location = newLoc;
                    curr._address = newAddress;
                    curr._description = newDescription;
                    curr._cagtegory = newCategory;
                    break;
                }

            }
            try { db.SubmitChanges(); return 1; }
            catch { return -1; }
        }
        #endregion
        #region get single object
        /// <summary>
        /// gets a single user
        /// </summary>
        /// <param name="id">users id</param>
        /// <returns>the user</returns>
        public clsUser getUserByID(int id)
        {
            try
            {
                List<clsUser> rtype = (from u in db.Users
                                       where id == u._ID
                                       select new clsUser(u._name, u._ID, u._password, u._authentication, u._initialLocation, u._notificationStyle, u._shouldBeNotified, u._email)).ToList();
                return rtype.ElementAt(0);
            }
            catch
            {
                return null;
            }
       
        }
        /// <summary>
        /// gets a single store
        /// </summary>
        /// <param name="name">sotre name</param>
        /// <returns>the store</returns>
        public clsStore getStoreByName(string name)
        {
            List<clsStore> rtype = (from u in db.Stores where u._name.Equals(name)
                                    select new clsStore(u._name, u._location, u._address, u._description, u._cagtegory)).ToList();
            return rtype.ElementAt(0);
        }
        /// <summary>
        /// gets users coupon by the primary key in the table
        /// </summary>
        /// <param name="id">users id</param>
        /// <param name="store">stores name</param>
        /// <param name="due">due date of the coupo</param>
        /// <param name="desc">description of the coupon</param>
        /// <returns>users coupon</returns>
        public clsUsersCoupon getUsersCouponByKey(int id,string store, DateTime due, string desc)
        {
            List<clsUsersCoupon> rtype = (from u in db.UsersCoupons where u._userID == id && u._store.Equals(store) && u._dueDate.Equals(due) && u._description.Equals(desc)
                                          select new clsUsersCoupon(u._userID, u._store, u._dueDate, u._description, u._dateUsed, u._rating,u._authKey,u._authenticatedByStore, u._isUsed)).ToList();
            return rtype.ElementAt(0);
        }
        /// <summary>
        /// gets a coupon by his key
        /// </summary>
        /// <param name="store">stores name</param>
        /// <param name="due">due date of the coupon</param>
        /// <param name="desc">description of the coupon</param>
        /// <returns>the coupon</returns>
        public clsCoupon getCouponByKey(string store,DateTime due, string desc)
        {
            List<clsCoupon> rtype = (from u in db.Coupons where u._store.Equals(store) && u._dueDate.Equals(due) && u._description.Equals(desc)
                                     select new clsCoupon(u._store, u._dueDate, u._avgRating, u._availableQuantity, u._category, u._description,(bool)u._isAuthByAdmin,(int)u._originalPrice,(int)u._newPrice)).ToList();
            return rtype.ElementAt(0);
        }
        #endregion
        #region get lists
        /// <summary>
        /// return all users in the system
        /// </summary>
        /// <returns>list of users</returns>
        public List<clsUser> getAllUsers()
        {
            List<clsUser> rtype = (from u in db.Users
                                   select new clsUser(u._name, u._ID, u._password, u._authentication, u._initialLocation, u._notificationStyle, u._shouldBeNotified, u._email)).ToList();
            return rtype;
        }
        /// <summary>
        /// get all users prefs
        /// </summary>
        /// <returns>all users pref in system</returns>
        public List<clsUsersPref> getAllUsersPref()
        {
            List<clsUsersPref> rtype = (from u in db.UsersPrefs
                                        select new clsUsersPref(u._ID,u._preference)).ToList();
            return rtype;
        }
        /// <summary>
        /// gets all stores in the system
        /// </summary>
        /// <returns>list of all stores</returns>
        public List<clsStore> getAllStores()
        {
            List<clsStore> rtype = (from u in db.Stores
                                    select new clsStore(u._name, u._location, u._address, u._description, u._cagtegory)).ToList();
            return rtype;
        }
        /// <summary>
        /// gets all the users coupons
        /// </summary>
        /// <returns>list of all users coupons</returns>
        public List<clsUsersCoupon> getAllUsersCoupon()
        {
            List<clsUsersCoupon> rtype = (from u in db.UsersCoupons
                                          select new clsUsersCoupon(u._userID, u._store, u._dueDate, u._description, u._dateUsed, u._rating, u._authKey, u._authenticatedByStore, u._isUsed)).ToList();
            return rtype;
        }
        /// <summary>
        /// get all the coupnons in the system
        /// </summary>
        /// <returns>list of coupons</returns>
        public List<clsCoupon> getAllCoupons()
        {
            List<clsCoupon> rtype = (from u in db.Coupons
                                     select new clsCoupon(u._store, u._dueDate, u._avgRating, u._availableQuantity, u._category, u._description, (bool)u._isAuthByAdmin, (int)u._originalPrice, (int)u._newPrice)).ToList();
            return rtype;
        }
        /// <summary>
        /// gets list of prefs of a single user
        /// </summary>
        /// <param name="id">users id</param>
        /// <returns>list of a given users prefs</returns>
        public List<clsUsersPref> getAllUsersPrefByID(int id)
        {
            List<clsUsersPref> rtype = (from u in db.UsersPrefs where id==u._ID
                                        select new clsUsersPref(u._ID, u._preference)).ToList();
            return rtype;
        }
        /// <summary>
        /// gets all the coupons that were orderd by single user
        /// </summary>
        /// <param name="id">users id</param>
        /// <returns>list of a given user coupons</returns>
        public List<clsUsersCoupon> getAllUsersCouponsByID(int id)
        {
            List<clsUsersCoupon> rtype = (from u in db.UsersCoupons
                                        where id == u   ._userID
                                          select new clsUsersCoupon(u._userID, u._store, u._dueDate, u._description, u._dateUsed, u._rating, u._authKey, u._authenticatedByStore, u._isUsed)).ToList();
            return rtype;
        }
        /// <summary>
        /// gets all the coupons that are orderd from a given store
        /// </summary>
        /// <param name="store">the stores name</param>
        /// <returns>list of usersCoupons</returns>
        public List<clsUsersCoupon> getUsersCouponsByStore(string store)
        {
            List<clsUsersCoupon> rtype = (from u in db.UsersCoupons
                                          where store.Equals(u._store)
                                          select new clsUsersCoupon(u._userID, u._store, u._dueDate, u._description, u._dateUsed, u._rating, u._authKey, u._authenticatedByStore, u._isUsed)).ToList();
            return rtype;
        }
        /// <summary>
        /// get all coupons of a given store
        /// </summary>
        /// <param name="store">the sore name</param>
        /// <returns>list of coupons</returns>
        public List<clsCoupon> getCouponsByStore(string store)
        {
            List<clsCoupon> rtype = (from u in db.Coupons
                                          where store.Equals(u._store)
                                     select new clsCoupon(u._store, u._dueDate, u._avgRating, u._availableQuantity, u._category, u._description, (bool)u._isAuthByAdmin, (int)u._originalPrice, (int)u._newPrice)).ToList();
            return rtype;
        }
        /// <summary>
        /// gets all the users with pref as their preference
        /// </summary>
        /// <param name="pref">the preference that we want</param>
        /// <returns>list of users</returns>
        public List<clsUser> getUsersByPref(string pref)
        {
            List<clsUser> rtype = (from u in db.Users join uc in db.UsersPrefs on u._ID equals uc._ID
                                     where pref.Equals(uc._preference)
                                   select new clsUser(u._name, u._ID, u._password, u._authentication, u._initialLocation, u._notificationStyle, u._shouldBeNotified, u._email)).ToList();
            return rtype;
        }
        /// <summary>
        /// gets all the un authorized coupons
        /// </summary>
        /// <returns> returns all the unauthorized coupons</returns>
        public List<clsCoupon> getCouponsNotAuthorized()
        {
            List<clsCoupon> rtype = (from u in db.Coupons
                                     where false == (bool)u._isAuthByAdmin
                                     select new clsCoupon(u._store, u._dueDate, u._avgRating, u._availableQuantity, u._category, u._description, (bool)u._isAuthByAdmin, (int)u._originalPrice, (int)u._newPrice)).ToList();
            return rtype;
        }

        #endregion
    }
}
