﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_BackEnd
{
    public class clsUsersPref
    {
        int _ID;
        string _pref;

        public int ID
        {
            get { return _ID; }
            set { }
        }
        public string pref
        {
            get { return _pref; }
            set { }
        }
        public clsUsersPref(int id, string pref)
        {
            _ID = id;
            _pref = pref;
        }
        public int getID() { return _ID; }
        public string getPref() { return _pref; }
    }
}
