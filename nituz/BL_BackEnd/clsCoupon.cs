﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Text;
using System.Threading.Tasks;

namespace BL_BackEnd
{
    public class clsCoupon
    {
        // fields: -- default visibilty -- 
        string _store;
        DateTime _dueDate;
        double _avgRating;
        int _availableQuntity;
        string _catagory;
        int _oldPrice;
        int _newPrice;
        string _description;
        bool _isAuthByAdmin;
        public string store
        {
            get { return _store; }
            set { }
        }
        public DateTime dueDate
        {
            get { return _dueDate; }
            set { }
        }
        public double avgRating
        {
            get { return _avgRating; }
            set { }
        }
        public int availableQuntity
        {
            get { return _availableQuntity; }
            set { }
        }
        public string catagory
        {
            get { return _catagory; }
            set { }
        }
        public int oldPrice
        {
            get { return _oldPrice; }
            set { }
        }
        public int newPrice
        {
            get { return _newPrice; }
            set { }
        }
        public string description
        {
            get { return _description; }
            set { }
        }
        public bool isAuthByAdmin
        {
            get { return _isAuthByAdmin; }
            set { }
        }


        // constructor:
        public clsCoupon (string storeName , DateTime dueDate , double avgRating,
            int avaiableQuntity, string catagory, string description, bool isAuthByAdmin,int oldPrice, int newPrice )
        {
            _store = storeName;
            _dueDate = dueDate;
            _avgRating = avgRating;
            _availableQuntity = avaiableQuntity;
            _catagory = catagory;
            _description = description;
            _isAuthByAdmin = isAuthByAdmin;
            _oldPrice = oldPrice;
            _newPrice = newPrice;
        }

        public string getStore() { return _store;}
        public DateTime getDueDate() { return _dueDate; }
        public double getAvgRating() { return _avgRating; }
        public int getAvailableQuntity() { return _availableQuntity; }
        public string getCatagory() { return _catagory; }
        public string getDescription() { return _description; }
        public bool getAuthByAdmin() { return _isAuthByAdmin; }
        public int getOldPrice() { return _oldPrice; }
        public int getNewPrice() { return _newPrice; }
    }
}
