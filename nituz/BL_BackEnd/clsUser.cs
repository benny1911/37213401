﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_BackEnd
{
    public class clsUser
    {
        // fields: -- default visibilty -- 
        private string _name;
        private int _ID;
        private string _password;
        private string _auth;
        private string _initialLocation;
        private int _notificationStyle;
        private bool _shouldBeNotified;
        private string _email;

        public string name
        {
            get { return _name; }
            set { }
        }
        public int ID
        {
            get { return _ID; }
            set { }
        }
        public string password
        {
            get { return _password; }
            set { }
        }
        public string auth
        {
            get { return _auth; }
            set { }
        }
        public string initialLocation
        {
            get { return _initialLocation; }
            set { }
        }
        public int notificationStyle
        {
            get { return _notificationStyle; }
            set { }
        }
        public bool shouldBeNotified
        {
            get { return _shouldBeNotified; }
            set { }
        }
        public string email
        {
            get { return _email; }
            set { }
        }
         
        // constructor:
        public clsUser(string username, int id, string password, string auth,string initialLocation, int? notificationStyle,bool shouldBeNotified, string email)
        {
            _name = username;
            _ID = id;
            _password = password;
            _auth = auth;
            _notificationStyle = (int)notificationStyle;
            _initialLocation = initialLocation;
            _shouldBeNotified = shouldBeNotified;
            _email = email;
        }
        public string getName() { return _name; }
        public int getID() { return _ID; }
        public string getPassword() { return _password; }
        public string getAuth() { return _auth; }
        public int getNotificationStyle() { return _notificationStyle; }
        public string getInitialLocation() { return _initialLocation; }
        public bool getShouldBeNotified() { return _shouldBeNotified; }
        public string getEmail() { return _email; }

    }
}
