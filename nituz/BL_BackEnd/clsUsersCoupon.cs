﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_BackEnd
{
    public class clsUsersCoupon
    {

 // fields: -- default visibilty -- 
        int _userID;
        string _store;
        DateTime _dueDate;
        string _description;
        DateTime _dateUsed;
        double _rating;
        string _authKey;
        bool _isAuthByStore;
        bool _isUsed;

        public int userID
        {
            get {return _userID; }
            set { }
        }
        public string store
        {
            get {return _store; }
            set { }
        }
        public DateTime dueDate
        {
            get {return _dueDate; }
            set { }
        }
        public string description
        {
            get {return _description; }
            set { }
        }
        public DateTime dateUsed
        {
            get {return _dateUsed; }
            set { }
        }
        public double rating
        {
            get {return _rating; }
            set { }
        }
        public string authKey
        {
            get {return _authKey; }
            set { }
        }
        public bool isAuthByStore
        {
            get { return _isAuthByStore; }
            set { }
        }

        public bool isUsed
        {
            get { return _isUsed; }
            set { }
        }


        // constructor:
        public clsUsersCoupon(int id, string storeName, DateTime dueDate, string description, DateTime? dateUsed, int? rating, string authKey, bool? isAuthByStore, bool isUsed)
        {
            _userID = id;
            _store = storeName;
            _dueDate = dueDate;
            _description = description;
            if (dateUsed != null)
                _dateUsed = (DateTime)dateUsed;
            else _dateUsed = new DateTime(9999, 1, 1);
            if (rating != null)
                _rating = (int)rating;
            else _rating = 0;
            _authKey = authKey;
            _isAuthByStore = (bool)isAuthByStore;
            _isUsed = false;
        }

        public int getUserID() { return _userID; }
        public string getStore() { return _store; }
        public DateTime getDueDate() { return _dueDate; }
        public string getDescription() { return _description; }
        public DateTime getDateUsed() { return _dateUsed; }
        public double getRating() { return _rating; }
        public string getAuthKey() { return _authKey; }
        public bool getIsAuthByStore() { return _isAuthByStore; }
        public bool getIsUsed() { return _isUsed; }

    }
}
