﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_BackEnd
{
    public class clsStore
    {
        // fields: -- default visibilty -- 
        string _name;
        string _location;
        string _address;
        string _description;
        string _category;
        public string name 
        {
            get { return _name; }
            set { }
        }
        public string location
        {
            get { return _location; }
            set { }
        }
        public string address
        {
            get { return _address; }
            set { }
        }
        public string description
        {
            get { return _description; }
            set { }
        }
        public string category
        {
            get { return _category; }
            set { }
        }
                // constructor:
        public clsStore(string storeName, string location, string address, string description, string category)
        {
            _name = storeName;
            _location = location;
            _address = address;
            _description = description;
            _category = category;
        }

        public string getName() { return _name; }
        public string getLocation() { return _location; }
        public string getAddress() { return _address; }
        public string getDescription() { return _description; }
        public string getCategory() { return _category; }
    }
}
