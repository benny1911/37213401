﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BL_BackEnd;

namespace BL
{
    public class AdminBL : UserBL
    {

        #region initialization
        //constructor:
        public AdminBL(IDAL itsDAL) : base(itsDAL) { }
        #endregion
        #region stores related
        public bool createNewStore(int id, string name, string location, string password, string address, string description, string category, string email)
        {
            int succsess = base.itsDAL.addStore(name, location, address, description, category);
            if (succsess == -1)
            {
                return false;
            }
            succsess = base.itsDAL.addUser(id, name, password, "S", 0, location, email); // 0 - store owner does not care about buying coupons
            if (succsess == -1)
            {
                return false;
            }
            return true;
        }

        public void removeStore(string storeName)
        {          
            clsStore store = base.itsDAL.getStoreByName(storeName);
            if (store == null)
            {
                return;
            }
            string location = store.getLocation();
            base.itsDAL.removeStore(storeName, location);
            List<clsUser> users = itsDAL.getAllUsers();
            int ID = 0;
            foreach (clsUser user in users)
            {
                if (user.getName().Equals(storeName) & user.getAuth().Equals("S"))
                {
                    ID = user.getID();
                    break;
                }
            }
            itsDAL.removeUser(ID);
        }
        #endregion
        #region coupon related
        public void confirmCoupon(string storeName, DateTime dueDate, string description){
            clsCoupon coupon = itsDAL.getCouponByKey(storeName, dueDate, description);
            if (coupon.getAvailableQuntity() == 0 | coupon.getDueDate()<DateTime.Today)
            {
                return;
            }
            itsDAL.updateCoupon(storeName, dueDate, description, dueDate, description, (int)coupon.getAvgRating(), coupon.getAvailableQuntity(), coupon.getCatagory(), coupon.getOldPrice(), coupon.getNewPrice(), true);
        }

        public bool addCoupon(string storeName, DateTime dueDate, string description, int quantity, string category, double originalPrice, double newPrice){
            // check for a valid category
            // if a test fails, return false
            int success = itsDAL.addCoupon(storeName, dueDate, description, quantity, category, originalPrice, newPrice, false);
            if (success == -1) // failed to add
            {
                return false;
            }
            return true;
        }

        public bool editCoupon(string storeName, DateTime dueDate, string description, DateTime newDueDate, string newDescription, int quantity, int newPrice){
            clsCoupon coupon = itsDAL.getCouponByKey(storeName, dueDate, description);
            int success = itsDAL.updateCoupon(storeName, dueDate, description, newDueDate, newDescription, (int)coupon.getAvgRating(), quantity, coupon.getCatagory(), coupon.getOldPrice(), newPrice, coupon.getAuthByAdmin());
            if (success == -1) 
            {
                return false;
            }
            return true;
        }

        public void removeCoupon(string storeName, DateTime dueDate, string description)
        {
            //itsDAL.removeCoupon(storeName, dueDate, description);
            clsCoupon coupon = itsDAL.getCouponByKey(storeName, dueDate, description);
            itsDAL.updateCoupon(storeName, dueDate, description, dueDate, description, (int)coupon.getAvgRating(), 0, coupon.getCatagory(), coupon.getOldPrice(), coupon.getNewPrice(), false);
            // the coupon is not actually deleted. instead, the available quantity is reduced to zero and the authorization set to false so no one can watch or buy it
            // need to think of a way to check if all bought coupons were used, than deleting all of it
        }

        public List<clsCoupon> showAllCoupons()
        {
            List<clsCoupon> coupons = itsDAL.getAllCoupons();
            return coupons;
        }

        public List<clsCoupon> showUnauthorizedCoupons()
        {
            List<clsCoupon> coupons = itsDAL.getAllCoupons();
            List<clsCoupon> ans = new List<clsCoupon>();
            foreach (clsCoupon coupon in coupons)
            {
                if (!coupon.getAuthByAdmin())
                {
                    ans.Add(coupon);
                }
            }
            return ans;
        }
        #endregion

        public List<clsCoupon> showCouponstoDelete()
        {
            List<clsCoupon> coupons = itsDAL.getAllCoupons();
            List<clsCoupon> ans = new List<clsCoupon>();
            foreach (clsCoupon coupon in coupons)
            {
                if (coupon.getAuthByAdmin() == true & coupon.getAvailableQuntity() > 0)
                {
                    ans.Add(coupon);
                }
            }
            return ans;
        }
    }
}
