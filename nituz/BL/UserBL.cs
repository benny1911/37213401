﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BL_BackEnd;
using System.Device.Location;
using System.Net;

namespace BL
{
    public class UserBL
    {
        // fields:
        protected IDAL itsDAL;

        #region initialization
        // constructor:
        public UserBL(IDAL iDal)
        {
            itsDAL = iDal;
        }
        #endregion
        #region login
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public string getAuthorization(int id, string password)
        {
            clsUser user = itsDAL.getUserByID(id);
            if (user == null)
            {
                return "x"; // error
            }
            string location = getLocation();
            // update user's location
            return user.getAuth();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="eMail"></param>
        public void restorePassword(int ID, string eMail)
        {
            clsUser user = itsDAL.getUserByID(ID);
            string password = user.getPassword();
            string name = user.getName();
            string subject = "Password restoration for coupons application"; // can enter a cool name for the app
            string msg = "Dear " + name + ",\n" + "As requested, here is your password:\n" + password + "\n\n" + "We hope you keep enjoying our services,\n" + "Support team :)\n";
            sendMail(eMail, subject, msg);
        }

        private void sendMail(string eMail, string subject, string msg)
        {
            try
            {

                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.To.Add(eMail);
                message.Subject = subject; // can enter a cool name for the app
                message.From = new System.Net.Mail.MailAddress("moshe.give.100@gmail.com"); // need to open a fake gmail address - must be gmail
                message.Body = msg;

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(message.From.Address, "nav,cht100")
                };

                smtp.EnableSsl = true;
                smtp.Send(message);

            }
            catch
            {
                // i dont care
            }
        }
        #endregion
        #region registration
        /// <summary>
        /// admin 5555 5555
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool createNewUser(int id, string name, string password,string mail)
        {
            string location = getLocation();
            int succsess = itsDAL.addUser(id, name, password, "U", 1, location,mail); // 1 for  notification by location
            if (succsess == -1)
            {
                return false;
            }
            return true;
        }

        protected string getLocation()
        {
            GeoCoordinateWatcher myGeoWatcher = new GeoCoordinateWatcher();
            myGeoWatcher.Start();
            return myGeoWatcher.Position.Location.ToString();

            /*
            System.Net.WebClient client = new System.Net.WebClient();
            string info = client.DownloadString("http://api.hostip.info/get_json.php");
            // the string should look like this: "{"country_name":"EUROPEAN UNION","country_code":"EU","city":"(Unknown city)","ip":"132.72.237.59"}"
            // this is what i based the following code on, trying to isolate the city
            info = info.Replace("\"", "");
            // string should look like this: "{country_name:EUROPEAN UNION,country_code:EU,city:(Unknown city),ip:132.72.237.59}"
            int index = info.IndexOf("city:") + 5;
            info = info.Substring(index);
            // string should look like this: "(Unknown city),ip:132.72.237.59}"
            index = info.IndexOf(",");
            info = info.Substring(0, index);
            // string should look like this: "(Unknown city)"
            return info;
             */
        }

        public string getMail (int id)
        {
            return itsDAL.getUserByID(id).getEmail();
        }

        #endregion
        #region serach coupon
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="preferences"> boolean value, select coupons by user pre-defined prefernces </param>
        /// <param name="storeName"> string of the store name, if does not wish to select by store, needs to be null </param>
        /// <param name="location"> location of coupon's advertiser, if does not wish to select by location, needs to be null </param>
        /// <returns></returns>
        public List<clsCoupon> searchCoupon(int id, bool preferences, string storeName, string location)
        {
            List<clsCoupon> allCoupons = itsDAL.getAllCoupons();
            List<clsCoupon> coupons = new List<clsCoupon>();
            foreach (clsCoupon c in allCoupons)
            {
                if (c.getAuthByAdmin() & c.getDueDate()>DateTime.Today)
                {
                    coupons.Add(c);
                }
            }
            if (preferences == true)
            {
                coupons = selectCouponsByPrefernces(id, coupons);
            }
            if (storeName != null)
            {
                coupons = selectCouponsByStore(storeName, coupons);
            }
            if (location != null)
            {
                coupons = selectCouponsByLocation(location, coupons);
            }
            return coupons;
        }

        private List<clsCoupon> selectCouponsByPrefernces(int id, List<clsCoupon> coupons)
        {
            List<clsUsersPref> prefs = itsDAL.getAllUsersPrefByID(id);
            HashSet<string> categories = new HashSet<string>();
            foreach (clsUsersPref pref in prefs)
            {
                categories.Add(pref.getPref());
            }
            List<clsCoupon> ans = new List<clsCoupon>();
            foreach (clsCoupon coupon in coupons)
            {
                string category = coupon.getCatagory();
                if (categories.Contains(category))
                {
                    ans.Add(coupon);
                }
            }
            return ans;
        }

        private List<clsCoupon> selectCouponsByStore(string storeName, List<clsCoupon> coupons)
        {
            List<clsCoupon> ans = new List<clsCoupon>();
            foreach (clsCoupon coupon in coupons)
            {
                if (coupon.getStore().Equals(storeName))
                {
                    ans.Add(coupon);
                }
            }
            return ans;
        }

        private List<clsCoupon> selectCouponsByLocation(string location, List<clsCoupon> coupons)
        {
            List<clsStore> stores = itsDAL.getAllStores();
            HashSet<string> storesNames = new HashSet<string>();
            foreach (clsStore store in stores)
            {
                if (store.getLocation().Equals(location))
                {
                    storesNames.Add(store.getName());
                }
            }
            List<clsCoupon> ans = new List<clsCoupon>();
            foreach (clsCoupon coupon in coupons)
            {
                if (storesNames.Contains(coupon.getStore()))
                {
                    ans.Add(coupon);
                }
            }
            return ans;
        }
        #endregion
        #region search store
        public List<clsStore> searchStores(int id, bool preferences, string location)
        {
            List<clsStore> stores = itsDAL.getAllStores();
            if (preferences == true)
            {
                stores = selectStoresByPreferences(id, stores);
            }
            if (location != null)
            {
                stores = selectStoresByLocation(location, stores);
            }
            return stores;
        }

        public List<clsStore> selectStoresByPreferences(int ID, List<clsStore> stores)
        {
            List<clsCoupon> coupons = searchCoupon(ID, true, null, null);
            HashSet<string> storesNames = new HashSet<string>();
            foreach (clsCoupon coupon in coupons)
            {
                storesNames.Add(coupon.getStore());
            }
            List<clsStore> ans = new List<clsStore>();
            foreach (clsStore store in stores)
            {
                if (storesNames.Contains(store.getName()))
                {
                    ans.Add(store);
                }
            }
            return ans;
        }

        private List<clsStore> selectStoresByLocation(string location, List<clsStore> stores)
        {
            // remove from stores all stores from different location
            List<clsStore> ans = new List<clsStore>();
            foreach (clsStore store in stores)
            {
                if (store.getLocation().Equals(location))
                {
                    ans.Add(store);
                }
            }
            return ans;
        }
        #endregion
        #region order coupon
        /// <summary>
        ///
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="coupon"></param>
        /// <returns> true if ordered successfully, false otherwise </returns>
        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.Synchronized)] // synchronized
        public bool buyCoupon(int ID, string storeName, DateTime dueDate, string description, string eMail)
        {
            clsCoupon coupon = itsDAL.getCouponByKey(storeName, dueDate, description);
            if (coupon.getAvailableQuntity() == 0 | DateTime.Compare(DateTime.Today, coupon.getDueDate()) > 0 | coupon.getAuthByAdmin() == false)
            {
                return false;
            }
            bool auth = coupon.getAuthByAdmin();
            if (coupon.getAvailableQuntity() == 1)
            {
                auth = false;
            }
            itsDAL.updateCoupon(storeName, dueDate, description, dueDate, description, (int)(coupon.getAvgRating()), coupon.getAvailableQuntity()-1, coupon.getCatagory(), coupon.getOldPrice(), coupon.getNewPrice(), auth);
            clsUser user = itsDAL.getUserByID(ID);
            if (user == null) return false;
            string name = user.getName();
            Random rand = new Random();
            int serial = rand.Next(1000000000);
            itsDAL.addUsersCoupon(ID, storeName, dueDate, description, serial.ToString()); // need to generate a random validation key. might use random integer and to string
            string subject = "Validation key for bought coupon";
            string msg = "Dear " + name + ",\n\n" + "Your validation key for your coupon from " + storeName + " for " + description + " is: " + serial.ToString();
            sendMail(eMail, subject, msg);

             subject = "Receipt for bought coupon";
             msg = "Dear " + name + ",\n\n" + "Your receipt  for your coupon from " + storeName + " for " + description + " is: " + coupon.getNewPrice();
            sendMail(eMail, subject, msg);

            return true;
        }
        #endregion
        #region rate coupon
        public int rateCoupon(int ID, string storeName, DateTime dueDate, string description, int rating)
        {
            clsUsersCoupon userCoupon = itsDAL.getUsersCouponByKey(ID, storeName, dueDate, description);
            if (userCoupon.getRating() != 0)
            {
                return -1;
            }
            itsDAL.updateUsersCoupon(ID, storeName, dueDate, description, userCoupon.getDateUsed(), rating, true, true);
            List<clsUsersCoupon> usersCoupons = itsDAL.getAllUsersCoupon();
            int counter = 0;
            int totalRating = 0;
            foreach (clsUsersCoupon coupon in usersCoupons)
            {
                if (coupon.getStore().Equals(storeName) & coupon.getDueDate().Equals(dueDate) & (coupon.getDescription().Equals(description)))
                {
                    counter++;
                    totalRating += (int)coupon.getRating();
                }
            }
            int newAvg = totalRating / counter; // counter cannot be a zero because at least one bought coupon was just updated
            clsCoupon couponToUpdate = itsDAL.getCouponByKey(storeName, dueDate, description);
            itsDAL.updateCoupon(storeName, dueDate, description, dueDate, description, newAvg, couponToUpdate.getAvailableQuntity(), couponToUpdate.getCatagory(), couponToUpdate.getOldPrice(), couponToUpdate.getNewPrice(), couponToUpdate.getAuthByAdmin());
            return 0;
        }
        #endregion
        #region preferences
        /// <summary>
        /// will set user preferences in the database
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="prefs"> array of strings of predetermined values (may be easy to use checkboxes to select them in GUI </param>
        public void setPreferences(int ID, string prefs)
        {
            List<clsUsersPref> preferences = itsDAL.getAllUsersPrefByID(ID);
            foreach (clsUsersPref pref in preferences)
            {
                itsDAL.removeUsersPref(ID, pref.getPref());
            }

            itsDAL.addUsersPref(ID, prefs);
            
        }
        #endregion
        #region orders
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public List<clsUsersCoupon> viewOrders(int ID)
        {
            return itsDAL.getAllUsersCouponsByID(ID);
        }
        public List<clsUsersCoupon> viewCloseToDueCoupons(int ID)
        {
            List<clsUsersCoupon> userCoupons = itsDAL.getAllUsersCouponsByID(ID);
            List<clsUsersCoupon> ans = new List<clsUsersCoupon>();
            DateTime today = DateTime.Today;
            foreach (clsUsersCoupon coupon in userCoupons)
            {
                if ((coupon.getDueDate() - today).TotalDays <= 7)
                {
                    ans.Add(coupon);
                }
            }
            return ans;
        }
        #endregion
        #region notifications preferences
        public bool setNotificationsAlarm(int ID, string notifyBy)
        {
            int notify = 0;
            if (notifyBy.Equals("Location"))
            {
                notify = 1;
            }
            else if (notifyBy.Equals("Preferences"))
            {
                notify = 2;
            }
            else if (notifyBy.Equals("Both"))
            {
                notify = 3;
            }
            else return false;
            clsUser user = itsDAL.getUserByID(ID);
            //`````````````````````````````````itsDAL.updateUser(ID, user.getName(), user.getPassword(), user.getAuth(), notify, user.getShouldBeNotified());
            return true;
        }

        public void changeNotificationsStatus(int ID)
        {
            clsUser user = itsDAL.getUserByID(ID);
            //------------------------------itsDAL.updateUser(ID, user.getName(), user.getPassword(), user.getAuth(), user.getNotificationStyle(), !user.getShouldBeNotified()); // simply reverses the boolean field "should be notified"
        }
        #endregion
        // add coupon from social network - unsupported operation
        public List<string> getCategories(int ID)
        {
            List<clsUsersPref> prefs = itsDAL.getAllUsersPrefByID(ID);
            List<string> ans = new List<string>();
            foreach (clsUsersPref up in prefs)
            {
                ans.Add(up.getPref());
            }
            return ans;
        }
    }
}
