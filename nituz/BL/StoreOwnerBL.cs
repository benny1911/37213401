﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BL_BackEnd;

namespace BL
{
    public class StoreOwnerBL : UserBL
    {
        #region initialization
        //constructor:
        public StoreOwnerBL(IDAL itsDAL) : base(itsDAL) { }
        #endregion

        public List<clsCoupon> showMyCoupons(string storeName)
        {
            return itsDAL.getCouponsByStore(storeName);
        }

        public bool updateStore(string storeName, string newName, string newLocation, string newAddress, string newDescription, string newCategory)
        {
            clsStore store = base.itsDAL.getStoreByName(storeName);
            string location = store.getLocation();
            if (base.itsDAL.updateStore(storeName, newName, newLocation, newAddress, newDescription, newCategory) == 1) // success value
            {
                return true;
            }
            return false;
        }

        public bool addCoupon(string storeName, DateTime dueDate, string description, int quantity, string category, double originalPrice, double newPrice)
        {
            // check for a valid category
            // if a test fails, return false
            int success = itsDAL.addCoupon(storeName, dueDate, description, quantity, category, originalPrice, newPrice, false);
            if (success == 0) // failed to add
            {
                return false;
            }
            return true;
        }

        public HashSet<clsCoupon> showBoughtCoupons(string storeName)
        {
            List<clsUsersCoupon> boughtCoupons = itsDAL.getAllUsersCoupon();
            HashSet<clsCoupon> ans = new HashSet<clsCoupon>();
            foreach (clsUsersCoupon boughtCoupon in boughtCoupons)
            {
                if (boughtCoupon.getStore().Equals(storeName))
                {
                    // maybe try and check first if that coupon exist in ans
                    DateTime dueDate = boughtCoupon.getDueDate();
                    string description = boughtCoupon.getDescription();
                    clsCoupon coupon = itsDAL.getCouponByKey(storeName, dueDate, description);
                    ans.Add(coupon);
                }
            }
            return ans;
        }

        public bool useCoupon(int buyerID, string serialKey, string storeName, DateTime dueDate, string description, string email)
        {
            clsUsersCoupon coupon = itsDAL.getUsersCouponByKey(buyerID, storeName, dueDate, description);
            if (!coupon.getAuthKey().Equals(serialKey) | coupon.getDateUsed() != null) // make sure it is initialized as null
            {
                return false;
            }
            itsDAL.updateUsersCoupon(buyerID, storeName, dueDate, description, DateTime.Now, 0, true, true);
            return true;
        }
    }
}
