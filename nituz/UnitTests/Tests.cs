﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools;
using NUnit.Framework;
using System.Data.SqlClient;
using DAL;

namespace UnitTests
{
    [TestFixture]
    public class Tests
    {

        [TestCase]
        public void addStoreTest()
        {
            int before = DatabaseTests.count("Stores");
            DatabaseTests.addStore("Horhe12", "a Nahash12", "food", "blabla", "Beer Sheva");
            int after = DatabaseTests.count("Stores");
            Assert.AreEqual(before, after - 1);
        }

        [TestCase]
        public void addExistStoreTest()
        {
            try
            {
                addStoreTest();
                // should produce Exception: "Violation of PRIMARY KEY"
            }
            catch (Exception)
            {
                //produce "true"
                Assert.AreEqual(0, 0);
                return;
            }
            //else:
            removeStoreTest();
        }

        [TestCase]
        public void addCouponTest()
        {

            int before = DatabaseTests.count("Coupons");
            DateTime d = new DateTime(2015, 4, 13);
            DatabaseTests.addCoupon("Horhe1", d, "w", 1, "a", 2, 1);
            int after = DatabaseTests.count("Coupons");
            Assert.AreEqual(before, after - 1);
        }

        [TestCase]
        public void removeCouponTest()
        {
            int before = DatabaseTests.count("Coupons");
            DateTime d = new DateTime(2015, 4, 13);
            DatabaseTests.removeCoupon("Horhe1", d, "w");
            int after = DatabaseTests.count("Coupons");
            Assert.AreEqual(before - 1, after);
        }

        [TestCase]
        public void addUserTest()
        {

            int before = DatabaseTests.count("Users");
            DatabaseTests.addUser(2, "2", "2", "2");
            int after = DatabaseTests.count("Users");
            Assert.AreEqual(before, after - 1);
        }

        [TestCase]
        public void removeUserTest()
        {
            int before = DatabaseTests.count("Users");
            DatabaseTests.removeUser(2);
            int after = DatabaseTests.count("Users");
            Assert.AreEqual(before - 1, after);
        }

        [TestCase]
        public void addUserPrefTest()
        {

            int before = DatabaseTests.count("UsersPref");
            DatabaseTests.addUserPref(0, "b");
            int after = DatabaseTests.count("UsersPref");
            Assert.AreEqual(before, after - 1);
        }

        [TestCase]
        public void removeUserPrefTest()
        {
            int before = DatabaseTests.count("UsersPref");
            DatabaseTests.removeUserPref(0, "b");
            int after = DatabaseTests.count("UsersPref");
            Assert.AreEqual(before - 1, after);
        }

        [TestCase]
        public void removeStoreTest()
        {
            int before = DatabaseTests.count("Stores");
            DatabaseTests.removeStore("Horhe12");
            int after = DatabaseTests.count("Stores");
            Assert.AreEqual(before - 1, after);
        }
    }
}
