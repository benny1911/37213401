﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using DAL;
namespace UnitTests
{
    static class DatabaseTests
    {
        public static SQL_DAL_implementation db = new SQL_DAL_implementation();

        public static void addStore(string storeName, string locName, string description, string category, string address)
        {
            string commandText = "INSERT INTO [dbo].[Stores] " + "([_name],[_location],[_address],[_description],[_cagtegory])" +
              "Values ('" + storeName + "', '" + locName + "', '" + address + "', '" + description + "', '" + category + "')";
            SQLWrite(commandText);
        }

        public static void removeStore(string storeName)
        {
            string commandText = "DELETE FROM [dbo].[Stores] " +
              "WHERE [Stores].[_name] = '" + storeName + "'";
            SQLWrite(commandText);
        }

        public static void addCoupon(string storeName, DateTime dueDate, string description, int quantity, string category, double originalPrice, double newPrice)
        {
            string commandText = "INSERT INTO [dbo].[Coupons] " + "([_store],[_dueDate],[_description],[_avgRating],[_availableQuantity],[_category],[_originalPrice],[_newPrice])" +
              "Values ('" + storeName + "', '" + dueDate + "', '" + description + "', '0', '" + quantity + "', '" + category + "', '" + originalPrice + "', '" + newPrice + "')";
            SQLWrite(commandText);
        }

        public static void removeCoupon(string storeName, DateTime dueDate, string description)
        {
            string commandText = "DELETE FROM [dbo].[Coupons] " +
              "WHERE [Coupons].[_store] = '" + storeName + "' AND [Coupons].[_dueDate] = '" + dueDate + "' AND [Coupons].[_description] = '" + description + "'";
            SQLWrite(commandText);
        }

        public static void addUser(int ID, string name, string password, string auth)
        {
            string commandText = "INSERT INTO [dbo].[Users] " + "([_ID],[_name],[_password],[_authentication],[_initialLocation],[_notificationStyle],[_shouldBeNotified],[_email])" +
              "Values ('" + ID + "', '" + name + "', '" + password + "', '" + auth + "', '" + 0 + "', '" + 0 + "', '" + 0 + "','benny@gmail.com')";
            SQLWrite(commandText);
        }

        public static void removeUser(int ID)
        {
            string commandText = "DELETE FROM [dbo].[Users] " +
              "WHERE [Users].[_ID] = '" + ID + "'";
            SQLWrite(commandText);
        }

        public static void addUserCoupon(int ID, string storeName, DateTime dueDate, string description, bool isUsed)
        {
            string commandText = "INSERT INTO [dbo].[UsersCoupons] " + "([_userID],[_store],[_dueDate],[_description],[_dateUsed],[_rating],[_authKey],[_authenticatedByStore],[_isUsed])" +
              "Values ('" + ID + "', '" + storeName + "', '" + dueDate + "', '" + description + "', NULL, NULL,123,0, '" + isUsed + "')";
            SQLWrite(commandText);
        }

        public static void removeUserCoupon(int ID, string storeName, DateTime dueDate, string description, bool isUsed)
        {
            string commandText = "DELETE FROM [dbo].[UsersCoupons] " +
              "WHERE [UsersCoupons].[_userID] = '" + ID + "' AND [UsersCoupons].[_store] ='" + storeName + "' AND [UsersCoupons].[_dueDate] ='" + dueDate + "' AND [UsersCoupons].[_description] ='" + description + "' AND [UsersCoupons].[_isUsed] ='" + isUsed + "'";
            SQLWrite(commandText);
        }

        public static void addUserPref(int ID, string pref)
        {
            string commandText = "INSERT INTO [dbo].[UsersPref] " + "([_ID],[_preference])" +
              "Values ('" + ID + "', '" + pref + "')";
            SQLWrite(commandText);
        }

        public static void removeUserPref(int ID, string pref)
        {
            string commandText = "DELETE FROM [dbo].[UsersPref] " +
              "WHERE [UsersPref].[_ID] = '" + ID + "' AND [UsersPref].[_preference] = '" + pref + "'";
            SQLWrite(commandText);
        }

        public static int count(string table)
        {
            db.SQLConnect();
            string commandText = "SELECT COUNT(*) FROM dbo." + table;
            SqlCommand command = new SqlCommand(commandText, db.myConnection);
            int x = (int)command.ExecuteScalar();//throws exceptions
            return x;

        }

        public static void SQLWrite(String commandText)
        {
            SqlCommand command = new SqlCommand(commandText, db.myConnection);
            command.ExecuteNonQuery();//throws exceptions
        }
    }
}
